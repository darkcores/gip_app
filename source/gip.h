/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAIN_H
#define MAIN_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <dirent.h>
#include <string.h>
#include <string>
#include <sstream>

#include <jwl/objects.h>

#include "settings.h"
#include "intro.h"
#include "menu.h"

#ifndef _WIN32
#include <X11/Xlib.h>
#endif 

/**
 * De main class die alle windows tekent. \n
 */
class Gip
{
	private:
		//debug vars
		sf::Text msg;
		sf::Clock fpstimer, timer;
		std::stringstream dss;
		std::string extraerr;

		std::atomic<bool> running;
		std::vector< sf::Drawable * > windows;
		Settings s;
		sf::ContextSettings context;

		void RenderThread();
		void LoadSettings();
	protected:
		/**
		 * Het venster. \n
		 */
		sf::RenderWindow window;
		/**
		 * Primaire kleur voor vensters. \n
		 */
		sf::Color primary;
		/**
		 * Secundaire kleur voor vensters. \n
		 */
		sf::Color secondary;
		/**
		 * Het geladen font. \n
		 */
		sf::Font font;
	public:
		/**
		 * Constructor. \n
		 */
		Gip();
		/**
		 * Constructor met argumenten. \n
		 */
		Gip(int, char **);
		/**
		 * Destructor. \n
		 */
		~Gip();

		/**
		 * Voer het programma uit. \n
		 */
		void Run();

		static std::mutex renderMutex;
		static void addwindow(std::vector<sf::Drawable *> &, sf::Drawable &);
		static void delwindow(std::vector<sf::Drawable *> &);
		static void clearwindow(std::vector<sf::Drawable *> &);
};

#endif
