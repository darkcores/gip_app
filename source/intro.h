/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef INTRO_H
#define INTRO_H

#include <jwl/window.h>
#include <jwl/loadbar.h>

#include "settings.h"

/**
 * Intro schermpje met laadbalk en logo. \n
 */
class Intro : public jwl::Window
{
	private:
		sf::Texture logo;
		sf::Sprite biglogo;
		sf::Text msg;

		jwl::LoadBar *lbar;
	public:
		Intro(sf::Font*, unsigned int, jwl::ColorPair);
		~Intro();

		virtual void LoadFunc();
};

#endif
