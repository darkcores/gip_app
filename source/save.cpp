/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "save.h"

enum KNOPPEN {
	pdf,
	tex,
	xdb,
	sqlite,
	sluiten
};

Save::Save(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	pdf = new jwl::Button("Als PDF", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*4),
				Colors.inverted());
	Foreground.push_back(pdf);
	Actions.push_back(pdf->getAction());
	Hover.push_back(pdf->getAction());
	tex = new jwl::Button("Als TEX", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*6),
				Colors.inverted());
	Foreground.push_back(tex);
	Actions.push_back(tex->getAction());
	Hover.push_back(tex->getAction());
	xdb = new jwl::Button("Als XDB", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*6),
				Colors.inverted());
	Foreground.push_back(xdb);
	Actions.push_back(xdb->getAction());
	Hover.push_back(xdb->getAction());
	sqlite = new jwl::Button("Als DB", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*8),
				Colors.inverted());
	Foreground.push_back(sqlite);
	Actions.push_back(sqlite->getAction());
	Hover.push_back(sqlite->getAction());
	sluiten = new jwl::Button("Terug", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*10),
				Colors.inverted());
	Foreground.push_back(sluiten);
	Actions.push_back(sluiten->getAction());
	Hover.push_back(sluiten->getAction());
}

Save::~Save()
{
	delete pdf;
	delete tex;
	delete xdb;
	delete sqlite;
	delete sluiten;
}

void Save::Click()
{
	switch(selected)
	{
		case KNOPPEN::pdf:
			save_as = new SaveAs(Font, FontSize, Colors, Filetypes::f_pdf);
			Gip::addwindow(Overlay, *save_as);
			save_as->Run(win);
			Gip::delwindow(Overlay);
			delete save_as;
			break;
		case KNOPPEN::tex:
			save_as = new SaveAs(Font, FontSize, Colors, Filetypes::f_tex);
			Gip::addwindow(Overlay, *save_as);
			save_as->Run(win);
			Gip::delwindow(Overlay);
			delete save_as;
			break;
		case KNOPPEN::xdb:
			save_as = new SaveAs(Font, FontSize, Colors, Filetypes::f_xdb);
			Gip::addwindow(Overlay, *save_as);
			save_as->Run(win);
			Gip::delwindow(Overlay);
			if(save_as->result)
			{
				msgbox = new jwl::Messagebox(Font, FontSize, Colors);
				msgbox->Text.setString("Gegevens opgeslagen!");
				Gip::addwindow(Overlay, *msgbox);
				msgbox->Run(win);
				Gip::delwindow(Overlay);
				delete msgbox;
			}
			delete save_as;
			break;
		case KNOPPEN::sqlite:
			save_as = new SaveAs(Font, FontSize, Colors, Filetypes::f_db);
			Gip::addwindow(Overlay, *save_as);
			save_as->Run(win);
			Gip::delwindow(Overlay);
			delete save_as;
			break;
		case KNOPPEN::sluiten:
			isRunning = false;
			break;
		default:
			break;
	}
}

void Save::LoadFunc()
{
	pdf->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*2), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*2));
	pdf->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		pdf->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	tex->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*4), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*4));
	tex->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		tex->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	xdb->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*6), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*6));
	xdb->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		xdb->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	sqlite->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*8), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*8));
	sqlite->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sqlite->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	sluiten->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*10), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*10));
	sluiten->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
}

void Save::EndFunc()
{
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		pdf->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		tex->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		xdb->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		sqlite->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
}
