/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MENU_H
#define MENU_H

#include <jwl/window.h>
#include <jwl/button.h>
#include <jwl/messagebox.h>

#include "settings.h"
#include "gip.h"
#include "import.h"
#include "group.h"
#include "save.h"

/**
 * Menuscherm met een aantal knoppen. \n
 */
class Menu : public jwl::Window
{
	private:
		sf::Texture bg;
		sf::Sprite background;

		//knoppen
		jwl::Button *import_w, *save_w, *exit_w, *group_w, *help_w;

		//messagebox
		jwl::Messagebox *msgbox;

		jwl::Window *w;
	public:
		Menu(sf::Font*, unsigned int, jwl::ColorPair);
		~Menu();

		virtual void Click();
		virtual void LoadFunc();
		virtual void EndFunc();
};

#endif
