/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "import.h"

enum KNOPPEN {
	sqlitedb,
	llnxdb,
	sluiten
};

Import::Import(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	sqlitedb = new jwl::Button("SQLite Database", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*4),
				Colors.inverted());
	Foreground.push_back(sqlitedb);
	Actions.push_back(sqlitedb->getAction());
	Hover.push_back(sqlitedb->getAction());

	llnxdb = new jwl::Button("XDB Bron", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*6),
				Colors.inverted());
	Foreground.push_back(llnxdb);
	Actions.push_back(llnxdb->getAction());
	Hover.push_back(llnxdb->getAction());

	sluiten = new jwl::Button("Terug", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*8),
				Colors.inverted());
	Foreground.push_back(sluiten);
	Actions.push_back(sluiten->getAction());
	Hover.push_back(sluiten->getAction());
}

Import::~Import()
{
	delete sqlitedb;
	delete llnxdb;
	delete sluiten;
}

bool Import::hasEnd (const std::string &fullString, const std::string &ending) {
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
	} else {
		return false;
	}
}

void Import::Click()
{
	switch(selected)
	{
		case KNOPPEN::sqlitedb:
			fileselect = new jwl::Filedialog(Font, FontSize / 4 * 3, Colors);
			Gip::addwindow(Overlay, *fileselect);
			fileselect->Run(win);
			if(fileselect->result)
			{
				if(hasEnd(fileselect->path, ".db"))
				{
					//do stuff
					data::filename = fileselect->path;
					sqlite3 *dbf;
					sqlite3_open(fileselect->path.c_str(), &dbf);
					msgbox = new jwl::Messagebox(Font, FontSize, Colors);
					if(data::loadFromDb(dbf))
						msgbox->Text.setString("Gegevens geimporteerd!");
					else
						msgbox->Text.setString("Kon de gegevens niet laden!");
					sqlite3_close(dbf);
					Gip::addwindow(Overlay, *msgbox);
					msgbox->Run(win);
					Gip::delwindow(Overlay);
					delete msgbox;
				}
				else
				{
					msgbox = new jwl::Messagebox(Font, FontSize, Colors);
					msgbox->Text.setString("Kies een .db bestand!");
					Gip::addwindow(Overlay, *msgbox);
					msgbox->Run(win);
					Gip::delwindow(Overlay);
					delete msgbox;
				}
			}
			Gip::delwindow(Overlay);
			delete fileselect;
			break;
		case KNOPPEN::llnxdb:
			fileselect = new jwl::Filedialog(Font, FontSize / 4 * 3, Colors);
			Gip::addwindow(Overlay, *fileselect);
			fileselect->Run(win);
			if(fileselect->result)
			{
				if(hasEnd(fileselect->path, ".xdb"))
				{
					//do stuff
					data::clearAll();
					FILE *fp = fopen(fileselect->path.c_str(), "rb");
					unsigned int count = 0;
					fread(&count, sizeof(unsigned int), 1, fp);
					data::workshops.resize(count);
					for (unsigned int i = 0; i < count; i++)
					{
						wrk_base tmp;
						fread(&tmp, sizeof(wrk_base), 1, fp);
						data::workshops[i] = tmp;
					};
					fread(&count, sizeof(unsigned int), 1, fp);
					data::leerlingen.resize(count);
					fread(&data::leerlingen[0], sizeof(std::vector<lln>::value_type), count, fp);
					fread(&count, sizeof(unsigned int), 1, fp);
					data::unsorted.resize(count);
					fread(&data::unsorted[0], sizeof(std::vector<unsigned short>::value_type), count, fp);
					fclose(fp);
					data::reloadWrk();
					msgbox = new jwl::Messagebox(Font, FontSize, Colors);
					msgbox->Text.setString("Geslaagd!");
					Gip::addwindow(Overlay, *msgbox);
					msgbox->Run(win);
					Gip::delwindow(Overlay);
					delete msgbox;
				}
				else
				{
					msgbox = new jwl::Messagebox(Font, FontSize, Colors);
					msgbox->Text.setString("Kies een .xdb bestand!");
					Gip::addwindow(Overlay, *msgbox);
					msgbox->Run(win);
					Gip::delwindow(Overlay);
					delete msgbox;
				}
			}
			Gip::delwindow(Overlay);
			delete fileselect;
			break;
		case KNOPPEN::sluiten:
			isRunning = false;
			break;
		default:
			break;
	}
}

void Import::LoadFunc()
{
	sqlitedb->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*2), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*2));
	sqlitedb->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sqlitedb->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	llnxdb->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*4), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*4));
	llnxdb->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		llnxdb->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	sluiten->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*6), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*6));
	sluiten->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
}

void Import::EndFunc()
{
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		sqlitedb->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		llnxdb->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
}
