/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GROUP_H
#define GROUP_H

#include "settings.h"
#include "gip.h"
#include "sort.h"
#include "group_editor.h"
#include "group_unsorted.h"
#include <jwl/window.h>
#include <jwl/button.h>
#include <jwl/messagebox.h>

class Group : public jwl::Window
{
	private:
		jwl::Button *create, *edit, *sort, *sluiten;
		jwl::Window *w;
		jwl::Messagebox *msgbox;
	public:
		Group(sf::Font*, unsigned int, jwl::ColorPair);
		~Group();

		virtual void Click();
		virtual void LoadFunc();
		virtual void EndFunc();

		void update(lln, wrk);
};

#endif
