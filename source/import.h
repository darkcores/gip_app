/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMPORT_H
#define IMPORT_H

#include <sqlite3.h>
#include <jwl/window.h>
#include <jwl/button.h>
#include <jwl/messagebox.h>
#include <jwl/filedialog.h>

#include "settings.h"
#include "gip.h"

/**
 * Scherm voor het importeren en selecteren van databanken. \n
 */
class Import : public jwl::Window
{
	private:
		jwl::Button *sqlitedb, *llnxdb, *sluiten;
		jwl::Filedialog *fileselect;
		jwl::Messagebox *msgbox;
		bool hasEnd(const std::string &, const std::string &);
	public:
		Import(sf::Font*, unsigned int, jwl::ColorPair);
		~Import();

		virtual void Click();
		virtual void LoadFunc();
		virtual void EndFunc();
};

#endif
