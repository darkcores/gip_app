/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "save_as.h"

SaveAs::SaveAs(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors, Filetypes f)
	: Messagebox::Messagebox(font, fontsize, colors)
{
	bestandstype = f;
	lblFilename = new jwl::Label("Bestandsnaam:", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(9*Settings::scale.x, Settings::scale.y*5),
				Colors);
	Foreground.push_back(lblFilename);

	txtFilename = new jwl::Textbox(Font, FontSize, sf::Vector2f(Settings::scale.x*7,
				Settings::scale.y), sf::Vector2f(13*Settings::scale.x, Settings::scale.y*5),
				Colors);
	Foreground.push_back(txtFilename);
	Actions.push_back(txtFilename->getAction());
}

SaveAs::~SaveAs()
{
	delete lblFilename;
	delete txtFilename;
}

void SaveAs::Click()
{
	Messagebox::Click();
}

void SaveAs::LoadFunc()
{
	switch(bestandstype)
	{
		case Filetypes::f_pdf:
			txtFilename->labeltxt = "Niet van toepassing";
			break;
		case Filetypes::f_tex:
			txtFilename->labeltxt = "Niet van toepassing";
			break;
		case Filetypes::f_xdb:
			txtFilename->labeltxt = "naam.xdb";
			break;
		case Filetypes::f_db:
			txtFilename->labeltxt = data::filename;
			break;
		default:
			break;
	}
	txtFilename->Refresh();
}

void SaveAs::EndFunc()
{
	if (!result)
		return;
	switch(bestandstype)
	{
		case Filetypes::f_pdf:
			genTex();
			tex2pdf();
			break;
		case Filetypes::f_tex:
			genTex();
			break;
		case Filetypes::f_xdb:
			writeXdb();
			break;
		case Filetypes::f_db:
			savedb();
			break;
		default:
			break;
	}
}

void SaveAs::savedb()
{
	sqlite3 *db = NULL;
	sqlite3_open(data::filename.c_str(), &db);
	if (db != NULL)
		data::saveToDb(db);
	else
		printf("Can't open database\n");
	sqlite3_close(db);
}

void SaveAs::genTex()
{
	std::vector< std::string > klasfiles, workfiles;
	//workshops uitvoeren
#ifndef _WIN32
	system("mkdir tex");
	system("mkdir tex/data");
	system("mkdir tex/data/workshops");
	system("mkdir tex/data/klassen");
	system("rm tex/data/workshops/*");
#else
	system("mkdir tex");
	system("mkdir tex\\data");
	system("mkdir tex\\data\\workshops");
	system("mkdir tex\\data\\klassen");
	system("del tex\\data\\workshops\\*");
#endif
	for (unsigned int i = 0; i < data::workshops.size(); i++)
	{
		workfiles.push_back(data::workshops[i].naam);
#ifdef _WIN32
		FILE *fp = fopen(std::string("tex\\data\\workshops\\" + std::string(data::workshops[i].naam) + ".csv").c_str(), "w");
#else
		FILE *fp = fopen(std::string("tex/data/workshops/" + std::string(data::workshops[i].naam) + ".csv").c_str(), "w");
#endif
		fprintf(fp, "Voormiddag,Namiddag\n");
		for (unsigned char n = 0; n < data::workshops[i].groep1.size() || n < data::workshops[i].groep2.size(); n++)
		{
			if (n >= data::workshops[i].groep2.size())
			{
				fprintf(fp, "%s %s, \n", data::getLeerling(data::workshops[i].groep1[n])->voornaam,
						data::getLeerling(data::workshops[i].groep1[n])->naam);
			}
			else if (n >= data::workshops[i].groep1.size())
			{
				fprintf(fp, " ,%s %s\n", data::getLeerling(data::workshops[i].groep2[n])->voornaam,
						data::getLeerling(data::workshops[i].groep2[n])->naam);
			}
			else
			{
				fprintf(fp, "%s %s,%s %s\n", data::getLeerling(data::workshops[i].groep1[n])->voornaam,
						data::getLeerling(data::workshops[i].groep1[n])->naam,
						data::getLeerling(data::workshops[i].groep2[n])->voornaam,
						data::getLeerling(data::workshops[i].groep2[n])->naam);
			}
		}
		fclose(fp);
	}
#ifndef _WIN32
	system("rm tex/data/klassen/*");
#else
	system("del tex\\data\\klassen\\*");
#endif
	for (unsigned int i = 0; i < data::leerlingen.size(); i++)
	{
		if (data::leerlingen[i].tg1 != 255)
		{
#ifdef _WIN32
			FILE *fp = fopen(std::string("tex\\data\\klassen\\" + std::string(data::leerlingen[i].klas) + ".csv").c_str(), "a");
#else
			FILE *fp = fopen(std::string("tex/data/klassen/" + std::string(data::leerlingen[i].klas) + ".csv").c_str(), "a");
#endif
			auto s = std::find(klasfiles.begin(), klasfiles.end(), data::leerlingen[i].klas);
			if (s == klasfiles.end())
			{
				klasfiles.push_back(data::leerlingen[i].klas);
				fprintf(fp, "Naam,Voormiddag,Namiddag\n");
			}
			fprintf(fp, "%s %s,%s,%s\n", data::leerlingen[i].voornaam, data::leerlingen[i].naam,
					data::getWorkshop(data::leerlingen[i].tg1)->naam,
					data::getWorkshop(data::leerlingen[i].tg2)->naam);
			fclose(fp);
		}
	}
	//aparte tex databestanden en tex bestanden klassen, workshops
	//
	//workshops to tex file
	FILE *fp = fopen("tex/workshops.tex", "w");
	fprintf(fp, "\\documentclass[12pt,a4paper]{article}\n");
	fprintf(fp, "\\usepackage[margin=1in]{geometry}\n");
	fprintf(fp, "\\usepackage{pgfplots}\n");
	fprintf(fp, "\\usepackage{pgfplotstable}\n");
	fprintf(fp, "\\begin{document}\n");
	fprintf(fp, "\\begin{center}\n");
	fprintf(fp, "\\begin{Large}\n");
	/***/
	for (unsigned int g = 0; g < workfiles.size(); g++)
	{
		fprintf(fp, "\\section*{%s}\n", workfiles[g].c_str());
		fprintf(fp, "\\pgfplotstabletypeset[col sep=comma,\n");
		fprintf(fp, "every head row/.style={before row=\\hline,after row=\\hline},\n");
		fprintf(fp, "every last row/.style={after row=\\hline},\n");
		fprintf(fp, "header=has colnames,\n");
		fprintf(fp, "columns={Voormiddag,Namiddag},\n");
		fprintf(fp, "columns/Voormiddag/.style={column type={|c|},string type},\n");
		fprintf(fp, "columns/Namiddag/.style={column type={c|},string type},\n");
		fprintf(fp, "]{tex/data/workshops/%s.csv}\n", workfiles[g].c_str());
		fprintf(fp, "\\pagebreak\n");
	}
	fprintf(fp, "\\end{Large}\n");
	fprintf(fp, "\\end{center}\n");
	fprintf(fp, "\\end{document}\n");
	fclose(fp);
	//leerlingen to tex files
	fp = fopen("tex/klassen.tex", "w");
	fprintf(fp, "\\documentclass[12pt,a4paper]{article}\n");
	fprintf(fp, "\\usepackage[margin=1in]{geometry}\n");
	fprintf(fp, "\\usepackage{pgfplots}\n");
	fprintf(fp, "\\usepackage{pgfplotstable}\n");
	fprintf(fp, "\\begin{document}\n");
	fprintf(fp, "\\begin{center}\n");
	fprintf(fp, "\\begin{Large}\n");
	/***/
	for (unsigned int g = 0; g < klasfiles.size(); g++)
	{
		fprintf(fp, "\\section*{%s}\n", klasfiles[g].c_str());
		fprintf(fp, "\\pgfplotstabletypeset[col sep=comma,\n");
		fprintf(fp, "every head row/.style={before row=\\hline,after row=\\hline},\n");
		fprintf(fp, "every last row/.style={after row=\\hline},\n");
		fprintf(fp, "header=has colnames,\n");
		fprintf(fp, "columns={Naam,Voormiddag,Namiddag},\n");
		fprintf(fp, "columns/Naam/.style={column type={|c},string type},\n");
		fprintf(fp, "columns/Voormiddag/.style={column type={|c|},string type},\n");
		fprintf(fp, "columns/Namiddag/.style={column type={c|},string type},\n");
		fprintf(fp, "]{tex/data/klassen/%s.csv}\n", klasfiles[g].c_str());
		fprintf(fp, "\\pagebreak\n");
	}
	fprintf(fp, "\\end{Large}\n");
	fprintf(fp, "\\end{center}\n");
	fprintf(fp, "\\end{document}\n");
	fclose(fp);
}

void SaveAs::tex2pdf()
{
#ifndef _WIN32
	system("pdflatex -output-directory=output ./tex/workshops.tex");
	system("pdflatex -output-directory=output ./tex/klassen.tex");
#else
	system("pdflatex -output-directory=output .\\tex\\workshops.tex");
	system("pdflatex -output-directory=output .\\tex\\klassen.tex");
#endif
}

void SaveAs::writeXdb()
{
	//create table
#ifdef _WIN32
	FILE *fp = fopen(std::string("output\\" + txtFilename->labeltxt).c_str(), "wb");
#else
	FILE *fp = fopen(std::string("output/" + txtFilename->labeltxt).c_str(), "wb");
#endif
	unsigned int count = data::workshops.size();
	fwrite(&count, sizeof(unsigned int), 1, fp);
	for (unsigned int i = 0; i < count; i++)
	{
		wrk_base tmp = data::workshops[i];
		fwrite(&tmp, sizeof(wrk_base), 1, fp);
	}
	count = data::leerlingen.size();
	fwrite(&count, sizeof(unsigned int), 1, fp);
	fwrite(&data::leerlingen[0], sizeof(std::vector<lln>::value_type), count, fp);
	count = data::unsorted.size();
	fwrite(&count, sizeof(unsigned int), 1, fp);
	fwrite(&data::unsorted[0], sizeof(std::vector<unsigned short>::value_type), count, fp);
	fclose(fp);
}

int SaveAs::xdb_callback(void *data, int argc, char **argv, char **azColName)
{
	(void)data;
	(void)argc;
	(void)argv;
	(void)azColName;
	return 0;
}

