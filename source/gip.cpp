/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "gip.h"

std::mutex Gip::renderMutex;

int main()
{
#ifndef _WIN32
	XInitThreads();
#endif
	Gip gip;
	try{
		gip.Run();
	}catch(int x) {
		std::cout << "Er is een probleem opgedoken\n \
			Het programma wordt afgesloten\n";
		switch(x)
		{
			default:
				break;
		}
	}
	return 0;
}

Gip::Gip()
{
	primary = sf::Color::Blue;
	secondary = sf::Color::White;
	std::cout << "======================================================\n";
	std::cout << "GIP 2015: DOE DAG\n(c) 2014 - 2015 Gerets Jorrit\n";
	std::cout << "This product is available under the GNU GPL v3 license\n";
	std::cout << "======================================================\n\n";
	running = true;
	LoadSettings();

	if(Settings::vsync)
	{
		window.setVerticalSyncEnabled(Settings::vsync);
	}
	else
	{
		window.setVerticalSyncEnabled(Settings::vsync);
		if(Settings::framelimit != 0)
			window.setFramerateLimit(Settings::framelimit);
	}

	if(Settings::AA != 0)
	{
		context.antialiasingLevel = Settings::AA;
	}

	font.loadFromFile(Settings::fontlocation);
}

Gip::~Gip()
{
	window.close();
}

void Gip::RenderThread()
{
	if(Settings::debug)
	{
		msg.setFont(font);
		msg.setCharacterSize(20);
		msg.setPosition(10, 10);
		msg.setColor(sf::Color::Yellow);
		fpstimer.restart();
		timer.restart();
	}
	
	sf::sleep(sf::milliseconds(100));
	while (running)
	{
		//sf::sleep(sf::milliseconds(5));
		Gip::renderMutex.lock();
		window.clear(secondary);
		for (unsigned int i = 0; i < windows.size(); i++)
		{
			try{
				window.draw(*windows[i]);
			}
			catch (int x)
			{
				std::cout << "Error in rendering, skipping object\n";
			}
		}
		Gip::renderMutex.unlock();
		try{
			if(Settings::debug)
				window.draw(msg);
			window.display();
			if(Settings::debug)
			{
				dss.str(std::string());
				dss << "FPS: ";
				dss << (1000.0 / fpstimer.getElapsedTime().asMilliseconds());
				dss << "\nTime Running: ";
				dss << timer.getElapsedTime().asSeconds() << " Seconds";
				msg.setString(dss.str());
				fpstimer.restart();
			}
		}
		catch (int x)
		{
			std::cout << "Skipping frame, cannot display\n";
		}
	}
}

void Gip::LoadSettings()
{
	std::fstream fs;
	bool exists = false;
	char settinsfile[] = "settings.ini";
	{
		//test of bestand bestaat
		struct dirent *item;
		DIR *dp;
		dp = opendir(".");
		while ((item = readdir(dp)))
		{
			if(strcmp(item->d_name, settinsfile) == 0)
			{
				exists = true;
				break;
			}
		}
		closedir(dp);
		//delete item;
	}
	if(!exists)
	{
		fs.open(settinsfile, std::ios::out);
		if(fs.is_open())
		{
			std::cout << "Creating new settings file\n";
			Settings::resolution = sf::VideoMode::getDesktopMode();
			fs << "Resolution = " << Settings::resolution.width << " X " << Settings::resolution.height << "\n";
			fs << "Antialiasing = " << Settings::AA << "\n";
			fs << "Vsync = " << (Settings::vsync ? "true" : "false") << "\n";
			fs << "Framerate = " << Settings::framelimit << "\n";
			fs << "UI-scale = " << Settings::scale.x << " " << Settings::scale.y << "\n";
			fs << "Font = " << Settings::fontlocation << "\n";
			fs << "ShowDebug = " << (Settings::debug ? "true\n" : "false\n");
			fs << "Fullscreen = " << (Settings::fullscreen ? "true\n" : "false\n");
			std::cout << "Resolution = " << Settings::resolution.width << " X " << Settings::resolution.height << "\n";
			std::cout << "Antialiasing = " << Settings::AA << "\n";
			std::cout << "Vsync = " << (Settings::vsync ? "true" : "false") << "\n";
			std::cout << "Framerate = " << Settings::framelimit << "\n";
			std::cout << "UI-scale = " << Settings::scale.x << " " << Settings::scale.y << "\n";
			std::cout << "Font = " << Settings::fontlocation << "\n";
			std::cout << "ShowDebug = " << (Settings::debug ? "true\n" : "false\n");
			std::cout << "Fullscreen = " << (Settings::fullscreen ? "true\n" : "false\n");
			std::cout << "ShowIntro = " << (Settings::intro ? "true\n" : "false\n");
		}
	}
	else
	{
		fs.open(settinsfile, std::ios::in);
		if(fs.is_open())
		{
			std::cout << "======Loading settings.ini======\n";
			std::string line, garbage, tmp;
			std::stringstream ss;

			std::getline(fs, line);
			ss << line;

			ss >> garbage >> garbage >> Settings::resolution.width >> garbage >> Settings::resolution.height;
			std::cout << "Loaded Resolution = " << Settings::resolution.width << " X " << Settings::resolution.height << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >> Settings::AA;
			std::cout << "AA settings = " << Settings::AA << "x\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >> tmp;
			(tmp == "true") ? Settings::vsync = true : Settings::vsync = false;
			std::cout << "Vsync = " << tmp << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >> Settings::framelimit;
			std::cout << "Framelimit = " << Settings::framelimit << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >> Settings::scale.x >> Settings::scale.y;
			std::cout << "UI scaling = " << Settings::scale.x << " " << Settings::scale.y << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >> Settings::fontlocation;
			std::cout << "Font = " << Settings::fontlocation << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >>tmp;
			(tmp == "true") ? Settings::debug = true : Settings::debug = false;
			std::cout << "ShowDebug = " << tmp << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >>tmp;
			(tmp == "true") ? Settings::fullscreen = true : Settings::fullscreen = false;
			std::cout << "Fullscreen = " << tmp << "\n";

			std::getline(fs, line);
			ss.clear();
			ss << line;
			ss >> garbage >> garbage >>tmp;
			(tmp == "true") ? Settings::intro = true : Settings::intro = false;
			std::cout << "ShowIntro = " << tmp << "\n";

			
			std::cout << "================================\n";
		}	
	}
	fs.close();
}

void Gip::Run()
{
	//Stijl 0 voor borderless fullscreen, helaas werkt borderless fullscreen niet meer met gnome 3.14
	if(Settings::fullscreen)
		window.create(Settings::resolution, "DOE DAG", sf::Style::Fullscreen, context);
	else
		window.create(Settings::resolution, "DOE DAG", sf::Style::Close, context);
	jwl::VideoSettings::Scale = Settings::scale;
	jwl::VideoSettings::Resolution = Settings::resolution;
	window.setActive(false); //anders werkt multithreading niet
	std::thread rndr (&Gip::RenderThread, this);

	jwl::ColorPair colors(sf::Color(0, 153, 255), sf::Color(255, 253, 253));

	jwl::Window *winn;
	if(Settings::intro)
	{
		winn = new Intro(&font, Settings::scale.x / 2, colors); 
		Gip::addwindow(windows, *winn);
		winn->Run(&window);
		Gip::delwindow(windows);
		delete winn;
	}

	winn = new Menu(&font, Settings::scale.x / 2, colors); 
	Gip::addwindow(windows, *winn);
	winn->Run(&window);
	Gip::delwindow(windows);
	delete winn;
	
	running = false;
	rndr.join();
	sf::sleep(sf::milliseconds(100));
}

void Gip::addwindow(std::vector<sf::Drawable *> &w_list, sf::Drawable &jwl_win)
{
	Gip::renderMutex.lock();
	w_list.push_back(&jwl_win);
	Gip::renderMutex.unlock();
}

void Gip::delwindow(std::vector<sf::Drawable *> &w_list)
{
	Gip::renderMutex.lock();
	w_list.pop_back();
	Gip::renderMutex.unlock();
}

void Gip::clearwindow(std::vector<sf::Drawable *> &w_list)
{
	Gip::renderMutex.lock();
	w_list.clear();
	Gip::renderMutex.unlock();
}
