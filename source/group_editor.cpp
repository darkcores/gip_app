/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "group_editor.h"

/**
 * Lijst met knoppen. \n
 */
enum KNOPPEN {
	sluiten,
	clear,
	vsdel,
	nsdel,
	istweede,
	isvm,
	isvs,
	isns,
	tbl_wrk
};

Group_Editor::Group_Editor(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	/* logo */
	bg.loadFromFile("background_menu.png");
	bg.setSmooth(true);
	background.setTexture(bg);
	background.setPosition(0, 0);
	background.setScale(Settings::scale.x / 60, Settings::scale.y / 60);
	Background.push_back(&background);

	sluiten = new jwl::Button("Terug", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*16),
				Colors);
	Foreground.push_back(sluiten);
	Actions.push_back(sluiten->getAction());
	Hover.push_back(sluiten->getAction());

	clear = new jwl::Button("Leegmaken", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*4),
				Colors);
	Foreground.push_back(clear);
	Actions.push_back(clear->getAction());
	Hover.push_back(clear->getAction());

	vsdel = new jwl::Button("VS Verwijderen", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*1),
				Colors);
	Foreground.push_back(vsdel);
	Actions.push_back(vsdel->getAction());
	Hover.push_back(vsdel->getAction());

	nsdel = new jwl::Button("NS Verwijderen", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*2.5),
				Colors);
	Foreground.push_back(nsdel);
	Actions.push_back(nsdel->getAction());
	Hover.push_back(nsdel->getAction());

	istweede = new jwl::Checkbox("Tweedejaars", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*6),
				Colors, false);
	Foreground.push_back(istweede);
	Actions.push_back(istweede->getAction());
	Hover.push_back(istweede->getAction());

	isvm = new jwl::Checkbox("Voormiddag", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*7.5),
				Colors, false);
	Foreground.push_back(isvm);
	Actions.push_back(isvm->getAction());
	Hover.push_back(isvm->getAction());

	isvs = new jwl::Checkbox("VS", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*9),
				Colors, false);
	Foreground.push_back(isvs);
	Actions.push_back(isvs->getAction());
	Hover.push_back(isvs->getAction());

	isns = new jwl::Checkbox("NS", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(25*Settings::scale.x, Settings::scale.y*10.5),
				Colors, false);
	Foreground.push_back(isns);
	Actions.push_back(isns->getAction());
	Hover.push_back(isns->getAction());

	tbl_wrk = new jwl::Table(sf::Vector2i(2, 24), Font, FontSize/5*4,
			sf::Vector2f(7*Settings::scale.x, 16*Settings::scale.y),
			sf::Vector2f(1*Settings::scale.x, 1*Settings::scale.y),
			Colors.inverted());
	tbl_wrk->setWidth(0, Settings::scale.x*1);
	tbl_wrk->setWidth(1, Settings::scale.x*6);
	Foreground.push_back(tbl_wrk);
	Actions.push_back(tbl_wrk->getAction());

	tbl_vs = new jwl::Table(sf::Vector2i(1, 24), Font, FontSize/5*4,
			sf::Vector2f(7*Settings::scale.x, 16*Settings::scale.y),
			sf::Vector2f(9*Settings::scale.x, 1*Settings::scale.y),
			Colors.inverted());
	Foreground.push_back(tbl_vs);
	Actions.push_back(tbl_vs->getAction());

	tbl_ns = new jwl::Table(sf::Vector2i(1, 24), Font, FontSize/5*4,
			sf::Vector2f(7*Settings::scale.x, 16*Settings::scale.y),
			sf::Vector2f(17*Settings::scale.x, 1*Settings::scale.y),
			Colors.inverted());
	Foreground.push_back(tbl_ns);
	Actions.push_back(tbl_ns->getAction());

	std::vector< std::string > tmp;
	tmp.push_back("ID");
	tmp.push_back("WORKSHOPS");
	tbl_wrk->lst.push_back(tmp);
	for (unsigned int i = 0; i < data::workshops.size(); i++)
	{
		tmp[0] = std::to_string(data::workshops[i].id);
		tmp[1] = data::workshops[i].naam;
		tbl_wrk->lst.push_back(tmp);
	}
	tbl_wrk->QuickRefresh();

	tmpwrk = NULL;
}

void Group_Editor::showMsg(const std::string msg)
{
	msgbox = new jwl::Messagebox(Font, FontSize, Colors);
	msgbox->Text.setString(msg);
	Gip::addwindow(Overlay, *msgbox);
	msgbox->Run(win);
	Gip::delwindow(Overlay);
}

Group_Editor::~Group_Editor()
{
	delete sluiten;
	delete clear;
	delete tbl_wrk;
	delete tbl_vs;
	delete tbl_ns;
	delete vsdel;
	delete nsdel;
	delete istweede;
	delete isvm;
	delete isvs;
	delete isns;
}

void Group_Editor::Click()
{
	switch(selected)
	{
		case KNOPPEN::sluiten:
			isRunning = false;
			break;
		case KNOPPEN::clear:
			msgbox = new jwl::Messagebox(Font, FontSize, Colors);
			msgbox->Text.setString("Weet u zeker dat u alle groepen\nwilt verwijderen?");
			Gip::addwindow(Overlay, *msgbox);
			msgbox->Run(win);
			Gip::delwindow(Overlay);
			if (msgbox->result)
			{
				data::clearWorkshops();
				tbl_vs->clear();
				tbl_ns->clear();
				tbl_vs->QuickRefresh();
				tbl_ns->QuickRefresh();
			}
			delete msgbox;
			break;
		case KNOPPEN::vsdel:
			{
			if (tmpwrk == NULL || tbl_vs->lst.size() < 2)
				break;
			auto *tmpln = data::getLeerling(tmpwrk->groep1[tbl_vs->selected - 1]);
			msgbox = new jwl::Messagebox(Font, FontSize, Colors);
			msgbox->Text.setString("Weet u zeker dat u " + std::string(tmpln->voornaam) + " \nwilt verwijderen uit " + std::string(tmpwrk->naam) +  "?");
			Gip::addwindow(Overlay, *msgbox);
			msgbox->Run(win);
			Gip::delwindow(Overlay);
			if (tbl_vs->lst.size() < 2 || tmpwrk == NULL || !msgbox->result)
			{
				delete msgbox;
				break;
			}
			data::getLeerling(tmpwrk->groep1[tbl_vs->selected - 1])->tg1 = 255;
			tmpwrk->groep1.erase(tmpwrk->groep1.begin() + tbl_vs->selected - 1);
			tbl_vs->lst.erase(tbl_vs->lst.begin() + tbl_vs->selected);
			tbl_vs->QuickRefresh();
			delete msgbox;
			break;
			}
		case KNOPPEN::nsdel:
			{
			if (tmpwrk == NULL || tbl_ns->lst.size() < 2)
				break;
			auto *tmpln = data::getLeerling(tmpwrk->groep2[tbl_vs->selected - 1]);
			msgbox = new jwl::Messagebox(Font, FontSize, Colors);
			msgbox->Text.setString("Weet u zeker dat u " + std::string(tmpln->voornaam) + " \nwilt verwijderen uit " + std::string(tmpwrk->naam) +  "?");
			Gip::addwindow(Overlay, *msgbox);
			msgbox->Run(win);
			Gip::delwindow(Overlay);
			if (tbl_vs->lst.size() < 2 || tmpwrk == NULL || !msgbox->result)
			{
				delete msgbox;
				break;
			}
			data::getLeerling(tmpwrk->groep2[tbl_vs->selected - 1])->tg2 = 255;
			tmpwrk->groep2.erase(tmpwrk->groep2.begin() + tbl_vs->selected - 1);
			tbl_vs->lst.erase(tbl_vs->lst.begin() + tbl_vs->selected);
			tbl_vs->QuickRefresh();
			delete msgbox;
			break;
			}
		case KNOPPEN::istweede:
			if (tmpwrk == NULL || tbl_vs->lst.size() < 2)
				break;
			tmpwrk->tweede = istweede->isChecked;
			break;
		case KNOPPEN::isvm:
			if (tmpwrk == NULL || tbl_vs->lst.size() < 2)
				break;
			tmpwrk->voormiddag = isvm->isChecked;
			if (isvm->isChecked)
			{
				isvs->set(true);
				isns->set(false);
				tmpwrk->eerste = true;
				tmpwrk->tweede = false;
			}
			break;
		case KNOPPEN::isvs:
			if (tmpwrk == NULL || tbl_vs->lst.size() < 2)
				break;
			isvm->set(false);
			tmpwrk->eerste = isvs->isChecked;
			break;
		case KNOPPEN::isns:
			if (tmpwrk == NULL || tbl_vs->lst.size() < 2)
				break;
			isvm->set(false);
			tmpwrk->tweede = isns->isChecked;	
			break;
		case KNOPPEN::tbl_wrk:
			{	
			tbl_vs->clear();
			tbl_ns->clear();
			std::vector< std::string > tmpstr;
			tmpstr.push_back("VOOR SPEELTIJD");
			tbl_vs->lst.push_back(tmpstr);
			tmpstr[0] = "NA SPEELTIJD";
			tbl_ns->lst.push_back(tmpstr);
			tmpwrk = data::getWorkshop(atoi(tbl_wrk->lst[tbl_wrk->selected][0].c_str()));
			for (unsigned int k = 0; k < tmpwrk->groep1.size(); k++)
			{
				tmpstr[0] = data::getLeerling(tmpwrk->groep1[k])->voornaam;
				tmpstr[0] += " ";
				tmpstr[0] += data::getLeerling(tmpwrk->groep1[k])->naam;
				tbl_vs->lst.push_back(tmpstr);
			}
			if (tmpwrk->voormiddag)
			{
				for (unsigned int k = 0; k < tmpwrk->groep1.size(); k++)
				{
					tmpstr[0] = data::getLeerling(tmpwrk->groep1[k])->voornaam;
					tmpstr[0] += " ";
					tmpstr[0] += data::getLeerling(tmpwrk->groep1[k])->naam;
					tbl_ns->lst.push_back(tmpstr);
				}
			}
			else
			{
				for (unsigned int k = 0; k < tmpwrk->groep2.size(); k++)
				{	
					tmpstr[0] = data::getLeerling(tmpwrk->groep2[k])->voornaam;
					tmpstr[0] += " ";
					tmpstr[0] += data::getLeerling(tmpwrk->groep2[k])->naam;
					tbl_ns->lst.push_back(tmpstr);
				}
			}
			istweede->set(tmpwrk->middenschool);
			isvm->set(tmpwrk->voormiddag);
			isvs->set(tmpwrk->eerste);
			isns->set(tmpwrk->tweede);
			tbl_vs->QuickRefresh();
			tbl_ns->QuickRefresh();
			break;
			}
		default:
			break;
	}
}

void Group_Editor::LoadFunc()
{
}

void Group_Editor::EndFunc()
{
}
