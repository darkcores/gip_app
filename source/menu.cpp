/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "menu.h"

/**
 * Lijst met knoppen. \n
 */
enum KNOPPEN {
	import_w,
	group_w,
	save_w,
	exit_w,
	help_w
};

Menu::Menu(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	/* logo */
	bg.loadFromFile("background_menu.png");
	bg.setSmooth(true);
	background.setTexture(bg);
	background.setPosition(0, 0);
	background.setScale(Settings::scale.x / 60, Settings::scale.y / 60);
	Background.push_back(&background);

	/* knoppen */
	import_w = new jwl::Button("Importeren", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*3),
				Colors.inverted());
	Foreground.push_back(import_w);
	Actions.push_back(import_w->getAction());
	Hover.push_back(import_w->getAction());

	group_w = new jwl::Button("Groepen", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*5),
				Colors.inverted());
	Foreground.push_back(group_w);
	Actions.push_back(group_w->getAction());
	Hover.push_back(group_w->getAction());

	save_w = new jwl::Button("Opslaan", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*9),
				Colors.inverted());
	Foreground.push_back(save_w);
	Actions.push_back(save_w->getAction());
	Hover.push_back(save_w->getAction());

	exit_w = new jwl::Button("Sluiten", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*11),
				Colors.inverted());
	Foreground.push_back(exit_w);
	Actions.push_back(exit_w->getAction());
	Hover.push_back(exit_w->getAction());

	help_w = new jwl::Button("Help", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*14),
				Colors.inverted());
	Foreground.push_back(help_w);
	Actions.push_back(help_w->getAction());
	Hover.push_back(help_w->getAction());
}

Menu::~Menu()
{
	delete import_w;
	delete group_w;
	delete save_w;
	delete exit_w;
	delete help_w;
}

void Menu::Click()
{
	switch(selected)
	{
		case KNOPPEN::import_w:
			w = new Import(Font, FontSize, Colors);
			Gip::addwindow(Overlay, *w);
			w->Run(win);
			Gip::delwindow(Overlay);
			delete w;
			break;
		case KNOPPEN::group_w:
			w = new Group(Font, FontSize, Colors);
			Gip::addwindow(Overlay, *w);
			w->Run(win);
			Gip::delwindow(Overlay);
			delete w;
			break;
		case KNOPPEN::save_w:
			w = new Save(Font, FontSize, Colors);
			Gip::addwindow(Overlay, *w);
			w->Run(win);
			Gip::delwindow(Overlay);
			delete w;
			break;
		case KNOPPEN::exit_w:
			msgbox = new jwl::Messagebox(Font, FontSize, Colors);
			msgbox->Text.setString("Wilt u sluiten?");
			Gip::addwindow(Overlay, *msgbox);
			msgbox->Run(win);
			if(msgbox->result)
				isRunning = false;
			Gip::delwindow(Overlay);
			delete msgbox;
			break;
		case KNOPPEN::help_w:
			msgbox = new jwl::Messagebox(Font, FontSize, Colors);
			msgbox->Text.setString("Importeren:\n\tImporteren van gegevens\nGroepen maken:\n\t\
Leerlingen in groepen plaatsen\nBewerken\n\tGeimporteerde gegevens bewerken\nOpslaan\n\t\
Opslaan en afprinten");
			Gip::addwindow(Overlay, *msgbox);
			msgbox->Run(win);
			Gip::delwindow(Overlay);
			delete msgbox;
			break;
		default:
			break;
	}
}

void Menu::LoadFunc()
{
	sf::sleep(sf::milliseconds(100));
	import_w->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*1), 
			sf::Vector2f(1*Settings::scale.x, Settings::scale.y*1));
	import_w->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		import_w->Effect(i);
		sf::sleep(sf::milliseconds(2));
	}
	group_w->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*3), 
			sf::Vector2f(1*Settings::scale.x, Settings::scale.y*3));
	group_w->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		group_w->Effect(i);
		sf::sleep(sf::milliseconds(2));
	}
	save_w->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*5), 
			sf::Vector2f(1*Settings::scale.x, Settings::scale.y*5));
	save_w->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		save_w->Effect(i);
		sf::sleep(sf::milliseconds(2));
	}
	exit_w->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*7), 
			sf::Vector2f(1*Settings::scale.x, Settings::scale.y*7));
	exit_w->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		exit_w->Effect(i);
		sf::sleep(sf::milliseconds(2));
	}
	help_w->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*16), 
			sf::Vector2f(1*Settings::scale.x, Settings::scale.y*16));
	help_w->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		help_w->Effect(i);
		sf::sleep(sf::milliseconds(2));
	}
}

void Menu::EndFunc()
{
	sf::CircleShape s(Settings::scale.x, 60);
	s.setPosition(Settings::scale.x * 15.5, Settings::scale.y * 19);
	s.setFillColor(Colors.Primary);
	Gip::clearwindow(Foreground);
	Gip::clearwindow(Overlay);
	Gip::addwindow(Foreground, s);
	for(float i = 19; i > 8.5; i -= 0.1)
	{
		s.setPosition(Settings::scale.x * 15.5, Settings::scale.y * i);
		sf::sleep(sf::milliseconds(3));
	}
	for(float i = 1; i < 32; i += 0.1)
	{
		s.setPosition(Settings::scale.x * (16.5 - i), Settings::scale.y * (9.5 - i));
		s.setRadius(Settings::scale.x * i);
		sf::sleep(sf::milliseconds(2));
	}
	Gip::clearwindow(Foreground);
}
