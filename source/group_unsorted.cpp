/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "group_unsorted.h"

/**
 * Lijst met knoppen. \n
 */
enum KNOPPEN {
	sluiten,
	addvs,
	addns,
	delvs,
	delns,
	limit_override,
	autoadd,
	tbl_lln,
	tbl_wrk
};

Group_Unsorted::Group_Unsorted(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	/* logo */
	bg.loadFromFile("background_bewerk.png");
	bg.setSmooth(true);
	background.setTexture(bg);
	background.setPosition(0, 0);
	background.setScale(Settings::scale.x / 60, Settings::scale.y / 60);
	Background.push_back(&background);

	sluiten = new jwl::Button("Terug", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(27*Settings::scale.x, Settings::scale.y*16),
				Colors);
	Foreground.push_back(sluiten);
	Actions.push_back(sluiten->getAction());
	Hover.push_back(sluiten->getAction());

	addvs = new jwl::Button("Toevoegen VS", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*1),
				Colors);
	Foreground.push_back(addvs);
	Actions.push_back(addvs->getAction());
	Hover.push_back(addvs->getAction());

	addns = new jwl::Button("Toevoegen NS", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(27*Settings::scale.x, Settings::scale.y*1),
				Colors);
	Foreground.push_back(addns);
	Actions.push_back(addns->getAction());
	Hover.push_back(addns->getAction());

	delvs = new jwl::Button("Verwijder VS", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*2.5),
				Colors);
	Foreground.push_back(delvs);
	Actions.push_back(delvs->getAction());
	Hover.push_back(delvs->getAction());

	delns = new jwl::Button("Verwijder NS", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(27*Settings::scale.x, Settings::scale.y*2.5),
				Colors);
	Foreground.push_back(delns);
	Actions.push_back(delns->getAction());
	Hover.push_back(delns->getAction());

	limit_override = new jwl::Checkbox("Negeer maximum groep", Font, FontSize, sf::Vector2f(Settings::scale.x*9,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*4),
				Colors, false);
	Foreground.push_back(limit_override);
	Actions.push_back(limit_override->getAction());
	Hover.push_back(limit_override->getAction());

	autoadd = new jwl::Button("Automatisch Toevoegen", Font, FontSize, sf::Vector2f(Settings::scale.x*9,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*5.5),
				Colors);
	Foreground.push_back(autoadd);
	Actions.push_back(autoadd->getAction());
	Hover.push_back(autoadd->getAction());

	tbl_lln = new jwl::Table(sf::Vector2i(3, 24), Font, FontSize/5*4,
			sf::Vector2f(11*Settings::scale.x, 16*Settings::scale.y),
			sf::Vector2f(1*Settings::scale.x, 1*Settings::scale.y),
			Colors.inverted());
	tbl_lln->setWidth(0, Settings::scale.x*1);
	tbl_lln->setWidth(1, Settings::scale.x*5);
	tbl_lln->setWidth(2, Settings::scale.x*5);
	Foreground.push_back(tbl_lln);
	Actions.push_back(tbl_lln->getAction());

	tbl_wrk = new jwl::Table(sf::Vector2i(2, 24), Font, FontSize/5*4,
			sf::Vector2f(8*Settings::scale.x, 16*Settings::scale.y),
			sf::Vector2f(13*Settings::scale.x, 1*Settings::scale.y),
			Colors.inverted());
	tbl_wrk->setWidth(0, Settings::scale.x*1);
	tbl_wrk->setWidth(1, Settings::scale.x*7);
	Foreground.push_back(tbl_wrk);
	Actions.push_back(tbl_wrk->getAction());

	lblwrk = new jwl::Label("Workshops bezet:", Font, FontSize, sf::Vector2f(Settings::scale.x*9,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*7),
				Colors);
	Foreground.push_back(lblwrk);

	vsstat = new jwl::Label("vsstat", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*8.5),
				Colors);
	Foreground.push_back(vsstat);

	nsstat = new jwl::Label("nsstat", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(27*Settings::scale.x, Settings::scale.y*8.5),
				Colors);
	Foreground.push_back(nsstat);

	lblln = new jwl::Label("Toegewezen keuzes: ", Font, FontSize, sf::Vector2f(Settings::scale.x*9,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*10),
				Colors);
	Foreground.push_back(lblln);

	vsln = new jwl::Label("tg1", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(22*Settings::scale.x, Settings::scale.y*11.5),
				Colors);
	Foreground.push_back(vsln);

	nsln = new jwl::Label("tg2", Font, FontSize, sf::Vector2f(Settings::scale.x*4,
				Settings::scale.y), sf::Vector2f(27*Settings::scale.x, Settings::scale.y*11.5),
				Colors);
	Foreground.push_back(nsln);

	updateTbl();
}

Group_Unsorted::~Group_Unsorted()
{
	delete sluiten;
	delete tbl_wrk;
	delete tbl_lln;
	delete addvs;
	delete addns;
	delete delvs;
	delete delns;
	delete autoadd;
	delete vsstat;
	delete nsstat;
	delete limit_override;
	delete vsln;
	delete nsln;
	delete lblwrk;
	delete lblln;
}

void Group_Unsorted::updateTbl()
{
	tbl_lln->clear();
	tbl_wrk->clear();

	std::vector< std::string > tmp;
	tmp.push_back("ID");
	tmp.push_back("VOORNAAM");
	tmp.push_back("NAAM");
	tbl_lln->lst.push_back(tmp);
	for (unsigned int i = 0; i < data::unsorted.size(); i++)
	{
		tmp[0] = std::to_string(data::unsorted[i]->id);
		tmp[1] = data::unsorted[i]->voornaam;
		tmp[2] = data::unsorted[i]->naam;
		tbl_lln->lst.push_back(tmp);
	}
	tbl_lln->QuickRefresh();

	tmp.pop_back();
	tmp[0] = "ID";
	tmp[1] = "WORKSHOPS";
	tbl_wrk->lst.push_back(tmp);
	for (unsigned int i = 0; i < data::workshops.size(); i++)
	{
		tmp[0] = std::to_string(data::workshops[i].id);
		tmp[1] = data::workshops[i].naam;
		tbl_wrk->lst.push_back(tmp);
	}
	tbl_wrk->QuickRefresh();

	if (tbl_wrk->lst.size() != 1)
	{
		auto *tmpwrk = data::getWorkshop(atoi(tbl_wrk->lst[tbl_wrk->selected][0].c_str()));
		vsstat->Text.setString("VS: " + std::to_string(tmpwrk->groep1.size()) + "/" + std::to_string(tmpwrk->maxAantal));
		nsstat->Text.setString("NS: " + std::to_string(tmpwrk->groep2.size()) + "/" + std::to_string(tmpwrk->maxAantal));
	}

	if (tbl_lln->lst.size() != 1)
	{	
		if (data::unsorted[tbl_lln->selected - 1]->tg1 == 255)
			vsln->Text.setString("Geen");
		else
			vsln->Text.setString(std::to_string(data::unsorted[tbl_lln->selected - 1]->tg1));
		if (data::unsorted[tbl_lln->selected - 1]->tg2 == 255)
			nsln->Text.setString("Geen");
		else
			nsln->Text.setString(std::to_string(data::unsorted[tbl_lln->selected - 1]->tg2));
	}
}

void Group_Unsorted::showMsg(const std::string msg)
{
	msgbox = new jwl::Messagebox(Font, FontSize, Colors);
	msgbox->Text.setString(msg);
	Gip::addwindow(Overlay, *msgbox);
	msgbox->Run(win);
	Gip::delwindow(Overlay);
}

void Group_Unsorted::Click()
{
	switch(selected)
	{
		case KNOPPEN::sluiten:
			isRunning = false;
			break;
		case KNOPPEN::addvs:
			{
			auto *tmpln = data::unsorted[tbl_lln->selected - 1];
			auto *tmpwrk = data::getWorkshop(atoi(tbl_wrk->lst[tbl_wrk->selected][0].c_str()));
			if (tmpwrk->groep1.size() >= tmpwrk->maxAantal && !limit_override->isChecked)
			{
				showMsg("Workshop groep 1 zit vol");
				break;
			}
			if (tmpln->tg1 != 255)
			{
				showMsg("Voor speeltijd voor leerling al toegewezen");
				break;
			}
			tmpln->tg1 = tmpwrk->id;
			tmpwrk->groep1.push_back(tmpln->id);
			showMsg("Toegevoegd!");
			}
			data::updateUnsorted();
			updateTbl();
			break;
		case KNOPPEN::addns:
			{
			auto *tmpln = data::unsorted[tbl_lln->selected - 1];
			auto *tmpwrk = data::getWorkshop(atoi(tbl_wrk->lst[tbl_wrk->selected][0].c_str()));
			if (tmpwrk->groep2.size() >= tmpwrk->maxAantal && !limit_override->isChecked)
			{
				showMsg("Workshop groep 2 zit vol");
				break;
			}
			if (tmpln->tg2 != 255)
			{
				showMsg("Voor speeltijd voor leerling al toegewezen");
				break;
			}
			tmpln->tg2 = tmpwrk->id;
			tmpwrk->groep2.push_back(tmpln->id);
			showMsg("Toegevoegd!");
			}
			data::updateUnsorted();
			updateTbl();
			break;
		case KNOPPEN::delvs:
			{
			auto *tmpln = data::unsorted[tbl_lln->selected - 1];
			if (tmpln->tg1 == 255)
			{
				showMsg("Leerling nog noet toegewezen VS");
				break;
			}
			auto *tmpwrk = data::getWorkshop(tmpln->tg1);
			tmpwrk->groep1.erase(std::remove(tmpwrk->groep1.begin(), tmpwrk->groep1.end(), tmpln->id), tmpwrk->groep1.end());
			tmpln->tg1 = 255;
			showMsg("Verwijderd!");
			}
			data::updateUnsorted();
			updateTbl();
			break;
		case KNOPPEN::delns:
			{
			auto *tmpln = data::unsorted[tbl_lln->selected - 1];
			if (tmpln->tg2 == 255)
			{
				showMsg("Leerling nog noet toegewezen NS");
				break;
			}
			auto *tmpwrk = data::getWorkshop(tmpln->tg2);
			tmpwrk->groep2.erase(std::remove(tmpwrk->groep2.begin(), tmpwrk->groep2.end(), tmpln->id), tmpwrk->groep2.end());
			tmpln->tg2 = 255;
			showMsg("Verwijderd!");
			}
			data::updateUnsorted();
			updateTbl();
			break;
		case KNOPPEN::tbl_lln:
			if (tbl_lln->lst.size() == 1)
				break;
			if (data::unsorted[tbl_lln->selected - 1]->tg1 == 255)
				vsln->Text.setString("Geen");
			else
				vsln->Text.setString(std::to_string(data::unsorted[tbl_lln->selected - 1]->tg1));
			if (data::unsorted[tbl_lln->selected - 1]->tg2 == 255)
				nsln->Text.setString("Geen");
			else
				nsln->Text.setString(std::to_string(data::unsorted[tbl_lln->selected - 1]->tg2));
			break;
		case KNOPPEN::tbl_wrk:
			{
			if (tbl_wrk->lst.size() == 1)
				break;
			auto *tmpwrk = data::getWorkshop(atoi(tbl_wrk->lst[tbl_wrk->selected][0].c_str()));
			vsstat->Text.setString("VS: " + std::to_string(tmpwrk->groep1.size()) + "/" + std::to_string(tmpwrk->maxAantal));
			nsstat->Text.setString("NS: " + std::to_string(tmpwrk->groep2.size()) + "/" + std::to_string(tmpwrk->maxAantal));
			break;
			}
		case KNOPPEN::autoadd:
			//showMsg("Niet geimplementeerd");
			data::unsortedLn();
			data::updateUnsorted();
			updateTbl();
			break;
		default:
			break;
	}
}

void Group_Unsorted::LoadFunc()
{
}

void Group_Unsorted::EndFunc()
{
}
