/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SAVE_AS_H
#define SAVE_AS_H

#include <dirent.h>

#include <jwl/messagebox.h>
#include <jwl/button.h>
#include <jwl/label.h>
#include <jwl/textbox.h>
#include "settings.h"

class wrk_data{
};

class SaveAs : public jwl::Messagebox
{
	private:
		static int xdb_callback(void *data, int argc, char **argv, char **azColName);
		jwl::Label *lblFilename;
		jwl::Textbox *txtFilename;
		Filetypes bestandstype;
		void genTex();
		void tex2pdf();
		void writeXdb();
		void savedb();
	public:
		SaveAs(sf::Font*, unsigned int, jwl::ColorPair, Filetypes);
		~SaveAs();

		virtual void LoadFunc();
		virtual void Click();
		virtual void EndFunc();
};

#endif
