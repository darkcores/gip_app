/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "settings.h"

/**
 * Variabelen initialiseren
 */
sf::VideoMode Settings::resolution;
unsigned int Settings::AA;
bool Settings::vsync;
unsigned int Settings::framelimit;
sf::Vector2f Settings::scale;
std::string Settings::fontlocation;
bool Settings::debug;
bool Settings::fullscreen;
bool Settings::intro;

Settings::Settings()
{
	Settings::resolution = sf::VideoMode::getDesktopMode();
	Settings::AA = 0;
	Settings::vsync = true;
	Settings::framelimit = 0;
	Settings::scale = sf::Vector2f(resolution.width / 32, resolution.height / 18);
	Settings::fontlocation = "SourceCodePro-Regular.otf";
	Settings::debug = false;
	Settings::fullscreen = true;
	Settings::intro = true;
}

Settings::~Settings()
{
}

lln::lln()
{
	tg1 = 255;
	tg2 = 255;
}

wrk::wrk()
{
	groep1.reserve(50);
	groep2.reserve(50);
}

bool wrk::operator< (const wrk &testval) const
{
	int open1 = 0;
	if (voormiddag)
		open1 += maxAantal - groep1.size();
	else
	{
		open1 += maxAantal - groep1.size();
		open1 += maxAantal - groep2.size();
	}
	int open2 = 0;
	if (testval.voormiddag)
		open2 += testval.maxAantal - testval.groep1.size();
	else
	{
		open2 += testval.maxAantal - testval.groep1.size();
		open2 += testval.maxAantal - testval.groep2.size();
	}
	return open1 < open2;
}

void wrk::operator=(const wrk_base &v_in)
{
	id = v_in.id;
	maxAantal = v_in.maxAantal;
	eerste = v_in.eerste;
	tweede = v_in.tweede;
	voormiddag = v_in.voormiddag;
	middenschool = v_in.middenschool;
	strcpy(naam, v_in.naam);
}

std::vector<lln> data::leerlingen;
std::vector<wrk> data::workshops;
std::vector<lln *> data::unsorted;
std::string data::filename;

data::data()
{
	workshops.reserve(500);
	leerlingen.reserve(500);
	unsorted.reserve(50);
}

int data::db_callback_update(void *data, int argc, char **argv, char **azColName)
{
	(void)data;
	(void)argc;
	(void)argv;
	(void)azColName;
	return 0;
}

int data::db_callback_lln(void *data, int argc, char **argv, char **azColName)
{
	(void)data;
	(void)argc;
	(void)azColName;
	lln tmp;
	tmp.id = atoi(argv[0]);
	strncpy(tmp.klas, argv[1], 6);
	strncpy(tmp.naam, argv[2], 25);
	strncpy(tmp.voornaam, argv[3], 25);
	tmp.tg1 = atoi(argv[4]);
	tmp.tg2 = atoi(argv[5]);
	for (unsigned char i = 0; i < 6; i++)
	{
		tmp.keuzes[i] = atoi(argv[6+i]);
		//printf("Keuze van lln: -%i-\n", tmp.keuzes[i]);
	}
	data::leerlingen.push_back(tmp);
	return 0;
}

int data::db_callback_wrk(void *data, int argc, char **argv, char **azColName)
{
	(void)data;
	(void)argc;
	(void)azColName;
	wrk tmp;
	tmp.id = atoi(argv[0]);
	strncpy(tmp.naam, argv[1], 25);
	tmp.maxAantal = atoi(argv[2]);
	(argv[3][0] == '1') ? tmp.eerste = true : tmp.eerste = false;
	(argv[4][0] == '1') ? tmp.tweede = true : tmp.tweede = false;
	(argv[5][0] == '1') ? tmp.voormiddag = true : tmp.voormiddag = false;
	(argv[6][0] == '1') ? tmp.middenschool = true : tmp.middenschool = false;
	data::workshops.push_back(tmp);
	//printf("Workshop ID: -%i-\n", tmp.id);
	return 0;
}

bool data::loadFromDb(sqlite3 *db)
{
	data::clearAll();
	int rc;
	char query1[] = "SELECT Leerlingen.stamnr, Leerlingen.KLAS, Leerlingen.NAAM, Leerlingen.VOORNAAM, Leerlingen.TG1, Leerlingen.TG2, Leerlingen.Keuze1, Leerlingen.Keuze2, Leerlingen.Keuze3, Leerlingen.Keuze4, Leerlingen.Keuze5, Leerlingen.Keuze6 FROM Leerlingen INNER JOIN Klassen ON Leerlingen.KLAS=Klassen.Klas ORDER BY Klassen.Voorkeur ASC";
	char query2[] = "SELECT Workshops.Volgnr, Workshops.WorkshopNaam, Workshops.maxAantal, Workshops.EersteWorkshop, Workshops.tweedeWorkshop, Workshops.GanseVoormiddag, Workshops.Middenschool FROM Workshops";
	rc = sqlite3_exec(db, query2, db_callback_wrk, NULL, NULL);
	if (rc != SQLITE_OK )
	{
		printf("Kan database niet lezen bij workshops. Error: %i\n", rc);
		return false;
	}
	rc = sqlite3_exec(db, query1, db_callback_lln, NULL, NULL);
	if (rc != SQLITE_OK )
	{
		printf("Kan database niet lezen bij leerlingen. Error: %i\n", rc);
		return false;
	}
	reloadWrk();
	return true;
}

void data::clearWorkshops()
{
	unsorted.clear();
	for (unsigned int i = 0; i < leerlingen.size(); i++)
	{
		leerlingen[i].tg1 = 255;
		leerlingen[i].tg2 = 255;
	}
	for(unsigned int i = 0; i < workshops.size(); i++)
	{
		workshops[i].groep1.clear();
		workshops[i].groep2.clear();;
	}
}

void data::clearAll()
{
	unsorted.clear();
	leerlingen.clear();
	workshops.clear();
	leerlingen.reserve(500);
	workshops.reserve(500);
	unsorted.reserve(50);
}

wrk *data::getWorkshop(unsigned char id)
{
	for (unsigned int i = 0; i < workshops.size(); i++)
	{
		if(workshops[i].id == id)
			return &workshops[i];
	}
	return NULL; //normally never do this, just to suppress warnings
}

lln *data::getLeerling(unsigned short id)
{
	for (unsigned int i = 0; i < leerlingen.size(); i++)
	{
		if(leerlingen[i].id == id)
			return &leerlingen[i];
	}
	return NULL;
}

void data::clearLln(lln &ln)
{
	if (ln.tg1 != 255)
	{
		wrk *tmp = getWorkshop(ln.tg1);
		auto v = std::find(tmp->groep1.begin(), tmp->groep1.end(), ln.id);
		if (v != tmp->groep1.end())
		{
			printf("Vector size 1: %lu\n", tmp->groep1.size());
			tmp->groep1.erase(v);
			printf("Vector size 2: %lu\n", tmp->groep1.size());
		}
		ln.tg2 = 255;
	}
	if (ln.tg2 != 255)
	{
		wrk *tmp = getWorkshop(ln.tg2);
		auto v = std::find(tmp->groep2.begin(), tmp->groep2.end(), ln.id);
		if (v != tmp->groep2.end())
			tmp->groep2.erase(v);
		ln.tg1 = 255;
	}
}

bool data::sort(lln &ln)
{
	//printf("Probeer student %i %s %s\n", ln.id, ln.voornaam, ln.naam);
	std::map<unsigned char, std::vector<unsigned char> > avail;
	clearLln(ln);

	bool vrmddg = false;
		
	//Test avail
	for (unsigned char i = 0; i < 6; i++)
	{
		wrk *tmp = getWorkshop(ln.keuzes[i]);
		if(tmp == NULL)
			continue;
		if(tmp->middenschool && ln.klas[0] == '2')
			continue;
		if (tmp->voormiddag && tmp->groep1.size() < tmp->maxAantal)
		{
			if (i < 3)
				vrmddg = true;
			else if(avail[1].size() < 3 && avail[2].size() < 3)
				vrmddg = true;
			avail[0].push_back(tmp->id);
		}
		if (tmp->groep1.size() < tmp->maxAantal && !tmp->voormiddag)
		{
			avail[1].push_back(tmp->id);
		}
		if (tmp->groep2.size() < tmp->maxAantal && !tmp->voormiddag)
		{
			avail[2].push_back(tmp->id);
		}
	}

	if (avail[0].size() > 0 && vrmddg)
	{
		ln.tg1 = avail[0][0];
		ln.tg2 = avail[0][0];
		getWorkshop(avail[0][0])->groep1.push_back(ln.id);
		return true;
	}
	if (avail[1].size() > 0 && avail[2].size() > 0)
	{
		/*
		for (unsigned int n = 0; n < avail[1].size(); n++)
		{
			for (unsigned int k = 0; k < avail[2].size(); k++)
			{
				if (avail[1][n] != avail[2][k])
				{
					ln.tg1 = avail[1][n];
					getWorkshop(avail[1][n])->groep1.push_back(ln.id);
					ln.tg2 = avail[2][k];
					getWorkshop(avail[2][k])->groep2.push_back(ln.id);
					return true;
				}
			}
		}
		*/
		unsigned int n = 0;
		unsigned int k = 0;
		bool f = true;
		bool s = true;
		do {
			if (avail[1][n] != avail[2][k])
			{
				ln.tg1 = avail[1][n];
				getWorkshop(avail[1][n])->groep1.push_back(ln.id);
				ln.tg2 = avail[2][k];
				getWorkshop(avail[2][k])->groep2.push_back(ln.id);
				return true;
			}
			if (n < avail[1].size() - 1)
				n++;
			else
				f = false;
			if (avail[1][n] != avail[2][k])
			{
				ln.tg1 = avail[1][n];
				getWorkshop(avail[1][n])->groep1.push_back(ln.id);
				ln.tg2 = avail[2][k];
				getWorkshop(avail[2][k])->groep2.push_back(ln.id);
				return true;
			}
			if (k < avail[2].size() - 1)
				k++;
			else
				s = false;
		}while(f || s);
	}
	if (avail[0].size() > 0)
	{
		ln.tg1 = avail[0][0];
		ln.tg2 = avail[0][0];
		getWorkshop(avail[0][0])->groep1.push_back(ln.id);
		return true;
	}
	//printf("Avail Size 1: %lu || 2: %lu\n", avail[1].size(), avail[2].size());
	return false;
}

int data::sortLln(void (*cb)(lln &ln, int num, int sort))
{
	data::clearWorkshops();
	unsigned short unsortable = 0;
	for (unsigned int i = 0; i < leerlingen.size(); i++)
	{
		cb(leerlingen[i], i + 1, unsortable);
		if (!data::sort(leerlingen[i]))
		{
			unsorted.push_back(&leerlingen[i]);
			unsortable++;
			clearLln(leerlingen[i]);
		}
	}
	return unsortable;
}

void data::unsortedLn()
{
	std::sort(workshops.begin(), workshops.end());
	for (unsigned int i = 0; i < unsorted.size(); i++)
	{
		unsigned char j = 0;
		while((unsorted[i]->tg1 == 255 || unsorted[i]->tg2 == 255) &&  j < workshops.size())
		{
			if (workshops[j].groep1.size() < workshops[j].maxAantal && unsorted[i]->tg1 == 255 && workshops[j].eerste)
			{
				unsorted[i]->tg1 = workshops[j].id;
				workshops[j].groep1.push_back(unsorted[i]->id);
				if (workshops[j].voormiddag)
					unsorted[i]->tg2 = workshops[j].id;
			}
			else if (workshops[j].groep2.size() < workshops[j].maxAantal && unsorted[i]->tg2 == 255 && workshops[j].tweede)
			{
				unsorted[i]->tg2 = workshops[j].id;
				workshops[j].groep2.push_back(unsorted[i]->id);
			}
			j++;
		}
	}
	updateUnsorted();
}

int data::wrkMinSize(unsigned char min)
{
	return min;
}

void data::reloadWrk()
{
	for (unsigned int i = 0; i < workshops.size(); i++)
	{
		workshops[i].groep1.clear();
		workshops[i].groep2.clear();
	}
	for (unsigned int i = 0; i < leerlingen.size(); i++)
	{
		wrk *w = getWorkshop(leerlingen[i].tg1);
		if (w != NULL)
			w->groep1.push_back(leerlingen[i].id);
		w = getWorkshop(leerlingen[i].tg2);
		if (w != NULL)
			w->groep2.push_back(leerlingen[i].id);
	}
	updateUnsorted();
}

void data::updateUnsorted()
{
	unsorted.clear();
	for (unsigned int i = 0; i < leerlingen.size(); i++)
	{
		if (leerlingen[i].tg1 == 255 || leerlingen[i].tg2 == 255)
			unsorted.push_back(&leerlingen[i]);
	}
}

void data::saveToDb(sqlite3 *db)
{
	(void)db;
	std::string sql = "";
	for (unsigned int i = 0; i < leerlingen.size(); i++)
	{
		sql = "UPDATE Leerlingen SET TG1=" + std::to_string(leerlingen[i].tg1) + ", TG2=" + std::to_string(leerlingen[i].tg2) + " WHERE Stamnr=" + std::to_string(leerlingen[i].id);
		int rc = sqlite3_exec(db, sql.c_str(), db_callback_update, NULL, NULL);
		if (rc != SQLITE_OK)
		{
			printf("Error writing leerlingen to database\n)");
			return;
		}
	}
	for (unsigned int i = 0; i < workshops.size(); i++)
	{
		sql = "UPDATE Workshops SET EersteWorkshop=" + std::string(workshops[i].eerste ? "'1'" : "'0'") +
			",TweedeWorkshop=" + std::string(workshops[i].tweede ? "'1'" : "'0'") +
			",GanseVoormiddag=" + std::string(workshops[i].voormiddag ? "'1'" : "'0'") +
			",Middenschool=" + std::string(workshops[i].middenschool ? "'1'" : "'0'") +
			" WHERE Volgnr='" + std::to_string(workshops[i].id) + "';";
		int rc = sqlite3_exec(db, sql.c_str(), db_callback_update, NULL, NULL);
		if (rc != SQLITE_OK)
		{
			printf("Error writing workshop to database\n)");
			return;
		}
	}
}
