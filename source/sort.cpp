/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sort.h"

jwl::Label *Sort::naam, *Sort::klas, *Sort::unsort, *Sort::teller;
jwl::LoadBar *Sort::laadbalk;

Sort::Sort(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Messagebox::Messagebox(font, fontsize, colors)
{
	naam = new jwl::Label("naamLeerling", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(9*Settings::scale.x, Settings::scale.y*5),
				Colors);
	Foreground.push_back(naam);
	klas = new jwl::Label("naamKlas", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(9*Settings::scale.x, Settings::scale.y*6),
				Colors);
	Foreground.push_back(klas);
	unsort = new jwl::Label("Unsortable", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(9*Settings::scale.x, Settings::scale.y*8),
				Colors);
	Foreground.push_back(unsort);
	teller = new jwl::Label("tellerLeerling", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(9*Settings::scale.x, Settings::scale.y*7),
				Colors);
	Foreground.push_back(teller);
	laadbalk = new jwl::LoadBar(sf::Vector2f(Settings::scale.x*10, Settings::scale.y * 1),
				sf::Vector2f(Settings::scale.x*9, Settings::scale.y*11), Font, FontSize,
				Colors);
	Foreground.push_back(laadbalk);
	laadbalk->updatePct(0.00);
}

Sort::~Sort()
{
	delete naam;
	delete klas;
	delete unsort;
	delete teller;
	delete laadbalk;
}

void Sort::LoadFunc()
{
	//int c1 = 0;
	int rc = 0;
	rc = data::sortLln(Sort::lnCallback);
	//printf("Niet gesorteerd: %i/%lu\n", rc, data::leerlingen.size());
	/*for (unsigned int i = 0; i < data::workshops.size(); i++)
	{
		c1 += data::workshops[i].groep1.size();
		printf("%-20s: 1:%lu 2: %lu\n", data::workshops[i].naam, data::workshops[i].groep1.size(), data::workshops[i].groep2.size());
	}*/
	//printf("Totaal G1: %i\n", c1);
}

void Sort::lnCallback(lln &ln, int num, int sort)
{
	//printf("Callback called\n");
	Sort::naam->labeltxt = "Naam: " + std::string(ln.voornaam) + " " + std::string(ln.naam);
	Sort::naam->Refresh();
	Sort::klas->labeltxt = "Klas: " + std::string(ln.klas);
	Sort::klas->Refresh();
	Sort::teller->labeltxt = std::string("Teller: ") + std::to_string(num) + "/" + std::to_string(data::leerlingen.size());
	Sort::teller->Refresh();
	Sort::unsort->labeltxt = std::string("Niet sorteerbaar: ") + std::to_string(sort);
	Sort::unsort->Refresh();
	Sort::laadbalk->updatePct((float)num / (float)data::leerlingen.size());
	sf::sleep(sf::milliseconds(2));
}
