/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <SFML/Graphics.hpp>
#include <atomic>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <sqlite3.h>

enum Filetypes {
	f_db,
	f_csv,
	f_pdf,
	f_tex,
	f_xdb
};

const int SWAP_VAR_MAXDEPTH = 15;

 /**
 * Class met de instellingen. \n
 */
class Settings
{
	public:
		/**
		 * Constructor. \n
		 */
		Settings();
		/**
		 * Destructor. \n
		 */
		~Settings();

		/**
		 * De resolutie van het venster. \n
		 */
		static sf::VideoMode resolution;
		/**
		 * Anti Aliasing installingen 0 - 16. \n
		 * 0 is uit
		 */
		static unsigned int AA;
		/**
		 * Vertical sync. \n
		 * Werkt niet met framerate limit
		 */
		static bool vsync;
		/**
		 * Limieteer het maximum frames / seconde. \n
		 * 0 is uit
		 */
		static unsigned int framelimit;
		/**
		 * Variabele voor UI schaal. \n
		 */
		static sf::Vector2f scale;
		/**
		 * String with the location of the font file. \n
		 */
		static std::string fontlocation;
		/**
		 * Toon debug info op het scherm. \n
		 */
		static bool debug;
		/**
		 * Is het venster fullscreen. \n
		 */
		static bool fullscreen;
		/**
		 * Intro animatie tonen. \n
		 */
		static bool intro;
};

class lln {
	public:
		lln();
		unsigned short id;
		unsigned char klasid;
		char voornaam[25], naam[25], klas[6];
		unsigned char keuzes[6];
		unsigned char tg1, tg2;
};

class wrk_base {
	public:
		unsigned char id, maxAantal;
		bool eerste, tweede, voormiddag, middenschool;
		char naam[25];
};

class wrk : public wrk_base {
	public:
		wrk();
		std::vector<unsigned short> groep1;
		std::vector<unsigned short> groep2;

		bool operator < (const wrk &testval) const;
		void operator = (const wrk_base &);
};

class data {
	private:
		//static std::string prevklas;
		//static bool groupklas;
		static int db_callback_update(void *data, int argc, char **argv, char **azColName);
		static int db_callback_lln(void *data, int argc, char **argv, char **azColName);
		static int db_callback_wrk(void *data, int argc, char **argv, char **azColName);
		static bool sort(lln &ln);
		static void clearLln(lln &ln);
	public:
		data();
		static std::vector<lln> leerlingen;
		static std::vector<wrk> workshops;
		static std::vector<lln *> unsorted;

		static bool loadFromDb(sqlite3 *db); //SQL order by voorkeur
		static void saveToDb(sqlite3 *db);
		static void clearWorkshops();
		static void clearAll();
		static void reloadWrk();
		static wrk *getWorkshop(unsigned char id);
		static lln *getLeerling(unsigned short id);
		static void updateUnsorted();
		/**
		 * Functie om de leerlingen te sorteren.
		 * Geeft 0 bij succes of n aantal mislukte pogingen.
		 */
		static int sortLln(void (*cb)(lln &, int, int));
		static void unsortedLn();
		static int wrkMinSize(unsigned char min);

		static std::string filename;
};
#endif
