/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "intro.h"

Intro::Intro(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	/* logo */
	logo.loadFromFile("logo.png");
	logo.setSmooth(true);
	biglogo.setTexture(logo);
	biglogo.setPosition(5 * Settings::scale.x, 3 * Settings::scale.y);
	biglogo.setScale(Settings::scale.x / 45, Settings::scale.y / 45);
	biglogo.setColor(sf::Color(255, 255, 255, 0));
	Background.push_back(&biglogo);

	lbar = new jwl::LoadBar(sf::Vector2f(Settings::scale.x*26, Settings::scale.y * 1),
				sf::Vector2f(Settings::scale.x*3, Settings::scale.y*15), Font, FontSize,
				Colors);
	Foreground.push_back(lbar);
	Actions.push_back(lbar->getAction());

	msg.setFont(*Font);
	msg.setCharacterSize(FontSize);
	msg.setColor(Colors.Primary);
	msg.setPosition(Settings::scale.x * 5, Settings::scale.y * 16);
	Foreground.push_back(&msg);
}

Intro::~Intro()
{
	delete lbar;
}

void Intro::LoadFunc()
{
	msg.setString("Starting program");
	sf::sleep(sf::milliseconds(150));
	for(float i = 0.00; i <= 0.20; i += 0.01)
	{
		lbar->updatePct(i);
		biglogo.setColor(sf::Color(255, 255, 255, 255*i));
		sf::sleep(sf::milliseconds(35));
	}
	msg.setString("Feeding unicorns");
	sf::sleep(sf::milliseconds(150));
	for(float i = 0.20; i <= 0.40; i += 0.01)
	{
		lbar->updatePct(i);
		biglogo.setColor(sf::Color(255, 255, 255, 255*i));
		sf::sleep(sf::milliseconds(25));
	}
	msg.setString("Loading libraries");
	sf::sleep(sf::milliseconds(150));
	for(float i = 0.40; i <= 0.60; i += 0.01)
	{
		lbar->updatePct(i);
		biglogo.setColor(sf::Color(255, 255, 255, 255*i));
		sf::sleep(sf::milliseconds(35));
	}
	msg.setString("Stalling for time");
	sf::sleep(sf::milliseconds(150));
	for(float i = 0.60; i >= 0.40; i -= 0.01)
	{
		lbar->updatePct(i);
		biglogo.setColor(sf::Color(255, 255, 255, 255*i));
		sf::sleep(sf::milliseconds(25));
	}
	msg.setString("Initialising core");
	sf::sleep(sf::milliseconds(150));
	for(float i = 0.40; i <= 0.80; i += 0.01)
	{
		lbar->updatePct(i);
		biglogo.setColor(sf::Color(255, 255, 255, 255*i));
		sf::sleep(sf::milliseconds(25));
	}
	msg.setString("Finishing startup");
	sf::sleep(sf::milliseconds(150));
	for(float i = 0.80; i <= 1.00; i += 0.01)
	{
		lbar->updatePct(i);
		biglogo.setColor(sf::Color(255, 255, 255, 255*i));
		sf::sleep(sf::milliseconds(25));
	}
	msg.setString("Starting");
	sf::sleep(sf::milliseconds(500));
	msg.setString("Starting ...");
	sf::sleep(sf::milliseconds(500));
	msg.setString("Starting ... ...");
	sf::sleep(sf::milliseconds(500));
	msg.setString("Starting ... ... ...");
	sf::sleep(sf::milliseconds(500));
	isRunning = false;
}
