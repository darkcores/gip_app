/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GROUP_EDITOR_H
#define GROUP_EDITOR_H

#include <stdlib.h>
#include <jwl/window.h>
#include <jwl/button.h>
#include <jwl/messagebox.h>
#include <jwl/inputbox.h>
#include <jwl/table.h>
#include <jwl/checkbox.h>

#include "settings.h"
#include "gip.h"

/**
 * Groepen editor. \n
 */
class Group_Editor : public jwl::Window
{
	private:
		sf::Texture bg;
		sf::Sprite background;

		wrk *tmpwrk;
		void showMsg(const std::string);

		//knoppen
		jwl::Button *sluiten, *vsdel, *nsdel, *clear;
		jwl::Checkbox *istweede, *isvm, *isvs, *isns;
		jwl::Table *tbl_wrk, *tbl_vs, *tbl_ns;

		//messagebox
		jwl::Messagebox *msgbox;
		jwl::Inputbox *inbox;
	public:
		Group_Editor(sf::Font*, unsigned int, jwl::ColorPair);
		~Group_Editor();

		virtual void Click();
		virtual void LoadFunc();
		virtual void EndFunc();
};

#endif
