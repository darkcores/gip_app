/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SAVE_H
#define SAVE_H

#include "gip.h"
#include "settings.h"
#include "save_as.h"

#include <jwl/window.h>
#include <jwl/button.h>
#include <jwl/messagebox.h>

class Save : public jwl::Window
{
	private:
		jwl::Button *pdf, *tex, *xdb, *sqlite, *sluiten;
		SaveAs *save_as;
		jwl::Messagebox *msgbox;
	public:
		Save(sf::Font*, unsigned int, jwl::ColorPair);
		~Save();

		virtual void Click();
		virtual void LoadFunc();
		virtual void EndFunc();
};

#endif
