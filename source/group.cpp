/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "group.h"

enum KNOPPEN {
	create,
	sort,
	edit,
	sluiten
};

Group::Group(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	create = new jwl::Button("Geneer Groepen", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*3),
				Colors.inverted());
	Foreground.push_back(create);
	Actions.push_back(create->getAction());
	Hover.push_back(create->getAction());

	sort = new jwl::Button("Sorteer extras", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*5),
				Colors.inverted());
	Foreground.push_back(sort);
	Actions.push_back(sort->getAction());
	Hover.push_back(sort->getAction());

	edit = new jwl::Button("Bewerk groepen", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*7),
				Colors.inverted());
	Foreground.push_back(edit);
	Actions.push_back(edit->getAction());
	Hover.push_back(edit->getAction());

	sluiten = new jwl::Button("Terug", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*9),
				Colors.inverted());
	Foreground.push_back(sluiten);
	Actions.push_back(sluiten->getAction());
	Hover.push_back(sluiten->getAction());
}

Group::~Group()
{
	delete create;
	delete sort;
	delete edit;
	delete sluiten;
}

void Group::Click()
{
	switch(selected)
	{
		case KNOPPEN::create:
			w = new Sort(Font, FontSize, Colors);
			Gip::addwindow(Overlay, *w);
			w->Run(win);
			Gip::delwindow(Overlay);
			delete w;
			break;
		case KNOPPEN::sort:
			if (data::unsorted.size() == 0)
			{
				msgbox = new jwl::Messagebox(Font, FontSize, Colors);
				msgbox->Text.setString("Geen leerling niet gesorteerd");
				Gip::addwindow(Overlay, *msgbox);
				msgbox->Run(win);
				Gip::delwindow(Overlay);
				delete msgbox;
			}
			else
			{
				//do unsorted stuff
				w = new Group_Unsorted(Font, FontSize, Colors);
				Gip::addwindow(Overlay, *w);
				w->Run(win);
				Gip::delwindow(Overlay);
			}
			break;
		case KNOPPEN::edit:
			w = new Group_Editor(Font, FontSize, Colors);
			Gip::addwindow(Overlay, *w);
			w->Run(win);
			Gip::delwindow(Overlay);
			delete w;
			break;
		case KNOPPEN::sluiten:
			isRunning = false;
			break;
		default:
			break;
	}
}

void Group::LoadFunc()
{
	create->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*2), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*2));
	create->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		create->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	sort->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*4), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*4));
	sort->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sort->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	edit->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*6), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*6));
	edit->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		edit->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	sluiten->MoveSet(sf::Vector2f(-5*Settings::scale.x, Settings::scale.y*8), 
			sf::Vector2f(7*Settings::scale.x, Settings::scale.y*8));
	sluiten->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
}

void Group::EndFunc()
{
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		create->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		sort->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		edit->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
	for (float i = 1.00; i > 0; i -= 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::microseconds(1500));
	}
}
